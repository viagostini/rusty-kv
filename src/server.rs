use tonic::Result;
use tonic::{transport::Server, Request, Response, Status};

use rusty_kv::greeter_server::{Greeter, GreeterServer};
use rusty_kv::{HelloResponse, HelloRequest};

pub mod rusty_kv {
    tonic::include_proto!("helloworld");
}

#[derive(Debug, Default)]
pub struct MyGreeter {}

#[tonic::async_trait]
impl Greeter for MyGreeter {
    async fn say_hello(&self, request: Request<HelloRequest>) -> Result<Response<HelloResponse>, Status> {
        println!("Got a request: {:?}", request);

        let reply = rusty_kv::HelloResponse {
            // We must use .into_inner() as the fields of gRPC requests and responses are private
            message: format!("Hello {}!", request.into_inner().name).into()
        };

        Ok(Response::new(reply))
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let addr = "[::1]:50051".parse()?;
    let greeter =MyGreeter::default();

    Server::builder().add_service(GreeterServer::new(greeter)).serve(addr).await?;

    Ok(())
}